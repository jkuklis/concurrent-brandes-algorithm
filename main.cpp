#include <iostream>
#include <fstream>
#include <unordered_map>
#include <deque>
#include <vector>
#include <list>
#include <queue>
#include <set>
#include <thread>
#include <map>
#include <mutex>
#include <stack>
#include <memory>

std::unordered_map<int, std::deque<int>> graph;
std::vector<int> keys;
std::vector<int> nodes;
std::map<int, double> betCon;

std::mutex mut;
std::map<int, std::unique_ptr<std::mutex>> mutexes;

int lastTaken = 0;

void add (int from, int to) {
    auto itFrom = graph.find(from);
    auto itTo = graph.find(to);
    if (itFrom == graph.end()) {
        graph.emplace(from, std::deque<int> {to});
        keys.push_back(from);
        nodes.push_back(from);
    }
    else {
        if (itFrom->second.empty())
            keys.push_back(from);
        itFrom->second.push_back(to);
    }
    if (itTo == graph.end()) {
        graph.emplace(to, std::deque<int> {});
        nodes.push_back(to);
    }
}

void print() {
    for (auto x : graph) {
        std::cout << x.first << ": ";
        for (auto y : x.second)
            std::cout << y << " ";
        std::cout << "\n";
    }
    std::cout << "\n";
    for (auto x : keys)
        std::cout << x << " ";
    std::cout << "\n";
    for (auto x : nodes)
        std::cout << x << " ";
    std::cout << "\n\n";
}

void threadFun () {
    while (true) {
        unsigned long n = nodes.size();
        int current;

        mut.lock();

        current = lastTaken;
        if (current != n)
            lastTaken++;

        mut.unlock();

        if (current == n)
            break;

        int s = nodes[current];

        std::queue<int> Q;
        std::stack<int> S;
        std::unordered_map<int, std::list<int>> P;
        std::unordered_map<int, int> d;
        std::unordered_map<int, int> sigma;
        std::unordered_map<int, double> delta;

        for (int i = 0; i < n; ++i) {
            sigma.emplace(nodes[i], 0);
            d.emplace(nodes[i], -1);
            delta.emplace(nodes[i], 0);
            P.emplace(nodes[i], std::list<int> {});
        }

        sigma[s] = 1;
        d[s] = 0;
        Q.push(s);

        while (!Q.empty()) {
            int v = Q.front();
            Q.pop();
            S.push(v);
            auto itv = d.find(v);
            for (auto w : graph.find(v)->second) {
                auto itw = d.find(w);
                if (itw->second < 0) {
                    Q.push(w);
                    itw->second = itv->second + 1;
                }

                if (itw->second == itv->second + 1) {
                    sigma.find(w)->second += sigma.find(v)->second;
                    P.find(w)->second.push_back(v);
                }
            }
        }

        while (!S.empty()) {
            int w = S.top();
            S.pop();
            for (auto v : P.find(w)->second) {
                delta.find(v)->second += (1 + delta.find(w)->second) * sigma.find(v)->second / sigma.find(w)->second;
            }
            if (w != s) {
                if (mutexes.find(w) == mutexes.end())
                    std::cout << "EAD";
                //mutexes.find(w)->second->lock();
                betCon.find(w)->second += delta.find(w)->second;
                //mutexes.find(w)->second->unlock();
            }
        }

    }
}

int main(int argc, char *argv[]) {
    int procCount;

    std::ifstream in;
    std::ofstream out;

    int from;
    int to;

    procCount = atoi(argv[1]);

    in.open(argv[2]);
    out.open(argv[3]);

    while (in >> from >> to)
        add(from, to);

    std::thread threads[procCount];

    for (int i = 0; i < keys.size(); ++i) {
        betCon.emplace(keys[i], 0);
        mutexes.emplace(keys[i], std::make_unique<std::mutex>());
    }

    for (int i = 0; i < procCount; ++i) {
        threads[i] = std::thread([]{ threadFun(); });
    }

    for (int i = 0; i < procCount; ++i) {
        threads[i].join();
    }

    for (auto x : betCon)
        out << x.first << " " << x.second << std::endl;
}